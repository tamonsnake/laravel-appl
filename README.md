<h1>Настройка проекта</h1>
<p><b>cmd</b></p>
<ol type="1">
    <li>composer install</li>
    <li>npm install</li>
</ol>
<p><b>.env</b></p>
<ol type="1">
    <li>Переименовать .env.example > .env</li>
    <li>В cmd выполнить: php artisan key:generate</li>
    <li>Для БД mysql(создать Базу Данных "имя_базы_данных"): <br>
                       DB_CONNECTION=mysql <br>
                       DB_HOST=127.0.0.1 <br>
                       DB_PORT=3306 <br>
                       DB_DATABASE=имя_базы_данных<br>
                       DB_USERNAME=логин_пользователя_mysql <br>
                       DB_PASSWORD=пароль_пользователя_mysql <br>
 
   </li>
   <li>SESSION_LIFETIME = 1440</li>
</ol>
<p><b>cmd</b></p>
<ol type="1">
    <li>php artisan migrate</li>
    <li>php artisan serve(В первом терминале)</li>
    <li>npm run watch(Во втором терминале)</li>
</ol>
