import {createApp} from 'vue'; // Vue 3 new syntax
import App from "./components/App"; // Root component

const app = createApp(App)

app.config.globalProperties.rus = false
app.mount('#app')
