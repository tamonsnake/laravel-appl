export default function () {
    let meta = Array.from(document.getElementsByName('csrf-token'))
    return meta[0].content
}
