import getCSRF from "./getCSRF";

export default async function(){
    let response = await fetch(
        `http://127.0.0.1:8000/identification`,
        {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': getCSRF()
            }
        })
    console.log(await response.json())
}
