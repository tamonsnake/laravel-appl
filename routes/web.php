<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoadFileController;
use App\Http\Controllers\IdentificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::post('/loadFile', [LoadFileController::class, 'store']);
Route::post('/identification', [IdentificationController::class, 'identification']);
