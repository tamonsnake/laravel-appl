<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Csv\Reader;
use League\Csv\Statement;

class LoadFileController extends Controller
{
    public function store(Request $request){
        $data = [];
        $csv = Reader::createFromPath($request->file, 'r');
        $csv->setHeaderOffset(0);
        $stmt = Statement::create();
        $records = $stmt->process($csv);
        foreach ($records as $record) {
            array_push($data, $record);
        }
        return json_encode($data);
    }
}
