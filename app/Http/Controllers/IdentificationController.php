<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class IdentificationController extends Controller
{
    public function identification(Request $request){
        if (session()->has('auth')){
            return json_encode(session_status());
        }
        else {
            session()->invalidate();
            $entity = User::create([
                'session_id' => Hash::make(session()->getId())
            ]);
            session()->put('auth', 1);
            return $entity;
        }
    }
}
